import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.groups.GroupFull;
import com.vk.api.sdk.queries.groups.GroupField;

import java.util.*;
import java.util.stream.Collectors;


public class Task1 {

    public static void main(String[] args) throws ClientException, ApiException {
        /**
         * Тут снизу написана солидная простыня кода, с тонной лямбда выражений, но расскажу сам принцип работы
         * (workflow).
         *  1) Сначала мы логинимся в апи ВК, и можем отправлять команды к ней
         *  2) Получаем список id моих первых пяти пабликов(самые популярные)
         *  3) Потом получаем для каждого id паблика его тематику (получаем список тематик)
         *  4) Кидаем запрос на получение id всех своих друзей
         *  5) Начинаем фильтровать список друзей по принципу
         *  id друга -> его паблики -> их темы -> совпадение тем с моими > 80% ? true : false
         *  6) Получаем финальный список друзей
         *  7) ?????
         *  8) PROFIT!!!!
         */
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);

        UserAuthResponse authResponse = vk.oauth()
                .userAuthorizationCodeFlow(1337, "SECRET", "https://oauth.vk.com/blank.html", "CODE")
                .execute();

        UserActor actor = new UserActor(authResponse.getUserId(), authResponse.getAccessToken());

        /*
        * Лист id-шников моих первых пяти пабликов ВК
        * */
        List<String> publicIdList = vk.users()
                .getSubscriptionsExtended(actor)
                .count(5)
                .execute()
                .getItems()
                .stream()
                .map(jsonObject -> jsonObject.get("id").getAsString())
                .collect(Collectors.toList());

        /*
        * Лист тематик моих первых 5 пабликов
        * */
        List<String> myListOfThemes = vk.groups()
                .getById(actor)
                .groupIds(publicIdList)
                .fields(GroupField.ACTIVITY)
                .execute()
                .stream()
                .map(GroupFull::getActivity)
                .collect(Collectors.toList());
        /*
        * Мой френдлист
        * */
        List<Integer> myFriendsList = vk.friends()
                .get(actor)
                .execute()
                .getItems();

        List<Integer> finalFriends = myFriendsList.stream()
                .filter(integer -> {
                    try {
                        return compareThemes(myListOfThemes, friendsIdsToThemes(friendIdtoPublicIds(integer, vk, actor), vk, actor));
                    } catch (ClientException e) {
                        e.printStackTrace();
                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                    return false;
                }).collect(Collectors.toList());

        System.out.println(finalFriends);
    }

    /**
     * Трансформатор  id друга в список айди пабликов
     * @param integer
     * @param vk
     * @param actor
     * @return
     * @throws ClientException
     * @throws ApiException
     */
    public static List<String> friendPublicToIds(Integer integer, VkApiClient vk, UserActor actor) throws ClientException, ApiException {
        return vk.groups()
                .get(actor)
                .userId(integer)
                .count(5)
                .execute()
                .getItems()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.toList());
    }

    /**
     * Трансформатор айди групп друга в список тем
     * @param myFriendsPublicIdList
     * @param vk
     * @param actor
     * @return
     * @throws ClientException
     * @throws ApiException
     */
    public static List<String> friendsIdsToThemes(List<String> myFriendsPublicIdList, VkApiClient vk, UserActor actor) throws ClientException, ApiException {
        return  vk.groups()
                .getById(actor)
                .groupIds(myFriendsPublicIdList)
                .fields(GroupField.ACTIVITY)
                .execute()
                .stream()
                .map(GroupFull::getActivity)
                .collect(Collectors.toList());
    }


    /**
     * Сравниваем, насколько одинаковы наши наборы тем с другом
     * @param myList
     * @param friendList
     * @return
     */
    public static boolean compareThemes(List<String> myList, List<String> friendList) {
        double count = 0;
        Set<String> set1 = new HashSet<>(myList);
        Set<String> set2 = new HashSet<>(friendList);

        Iterator<String> it = set1.iterator();

        while (it.hasNext()){
            String s = it.next();

            if (set2.contains(s)){
                count++;
            }
        }
        if(count/(double)set1.size() * 100.0 >= 80.0) {
            return true;
        }
        return false;
    }
}
